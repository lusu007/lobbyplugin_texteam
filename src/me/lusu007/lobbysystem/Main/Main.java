package me.lusu007.lobbysystem.Main;

import de.slikey.effectlib.EffectLib;
import de.slikey.effectlib.EffectManager;

import me.lusu007.lobbysystem.API.MySQL.MySQL;
import me.lusu007.lobbysystem.API.MySQL.MySQLFile;
import me.lusu007.lobbysystem.Commands.*;
import me.lusu007.lobbysystem.Commands.GameMode;
import me.lusu007.lobbysystem.Features.DoubleJump;
import me.lusu007.lobbysystem.Features.JumpPlates;
import me.lusu007.lobbysystem.ItemManager.*;
import me.lusu007.lobbysystem.Listener.*;

import org.bukkit.*;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Lukas on 04.01.2015.
 *
 */
public class Main extends JavaPlugin {

    /**
     * Permissions:
     * - server.join.message
     * - server.leave.message
     * - server.build.break
     * - server.build.place
     * - server.inventory.drag
     * - server.pl.command
     * - server.doublejump.doubleJump
     * - server.doublejump.groundPound
     * - server.schild
     * - server.nicker
     * - server.flugstab
     * - server.settings
     * - server.set.gamemode
     * - server.command.set
     * - server.teleport
     * - server.build.mode
     * - server.bypass.schild
     */

    //==========[Strings]==========

    public String cmdprefix = ChatColor.DARK_BLUE + "[LobbySystem] ";
    public String noperm = ChatColor.DARK_RED + "ERROR Keine Rechte!";

    //==========[List]==========

    public static ArrayList<String> onlineplayer = new ArrayList<String>();

    public static ArrayList<String> flying = new ArrayList<String>();
    public static ArrayList<String> saving = new ArrayList<String>();

    public static ArrayList<String> hidden1 = new ArrayList<String>();
    public static ArrayList<String> hidden2 = new ArrayList<String>();

    //==========[Integer]==========

    public int timecountdown;
    public int tablistsync;

    //==========[Boolean]==========

    public boolean maintenance;

    //==========[Hashmap]==========

    public static HashMap<String, BukkitRunnable> jumper = new HashMap<String, BukkitRunnable>();
    public static HashMap<String, BukkitRunnable> schild = new HashMap<String, BukkitRunnable>();

    //==========[EffectManager]==========

    public static EffectManager em;

    @Override
    public void onEnable() {
        this.getLogger().info(cmdprefix + "Plugin aktiviert.");

        Bukkit.getServer().setDefaultGameMode(org.bukkit.GameMode.ADVENTURE);

        //Erstellt MySQl Cfg
        MySQLFile file = new MySQLFile();
        file.setStandard();
        file.readData();

        MySQL.connect();

        em = new EffectManager(EffectLib.instance());

        //Events registrieren
        new PlayerJoinEvent_Listener(this);
        new PlayerQuitEvent_Listener(this);
        new BlockBreakEvent_Listener(this);
        new BlockBurnEvent_Listener(this);
        new BlockDamageEvent_Listener(this);
        new BlockFadeEvent_Listener(this);
        new BlockFormEvent_Listener(this);
        new BlockGrowEvent_Listener(this);
        new BlockPlaceEvent_Listener(this);
        new BlockSpreadEvent_Listener(this);
        new EntityBlockFormEvent_Listener(this);
        new LeavesDecayEvent_Listener(this);
        new EnchantItemEvent_Listener(this);
        new PrepareItemEnchantEvent_Listener(this);
        new CreeperPowerEvent_Listener(this);
        new EntityBreakDoorEvent_Listener(this);
        new EntityChangeBlockEvent_Listener(this);
        new EntityCombustByBlockEvent_Listener(this);
        new EntityCombustByEntityEvent_Listener(this);
        new EntityCombustEvent_Listener(this);
        new EntityCreatePortalEvent_Listener(this);
        new EntityDamageByBlockEvent_Listener(this);
        new EntityDamageByEntityEvent_Listener(this);
        new EntityDamageEvent_Listener(this);
        new EntityExplodeEvent_Listener(this);
        new EntityInteractEvent_Listener(this);
        new EntityPortalEvent_Listener(this);
        new EntityPortalExitEvent_Listener(this);
        new EntityRegainHealthEvent_Listener(this);
        new EntityShootBowEvent_Listener(this);
        new EntityTeleportEvent_Listener(this);
        new ExplosionPrimeEvent_Listener(this);
        new FoodLevelChangeEvent_Listener(this);
        new ItemSpawnEvent_Listener(this);
        new PigZapEvent_Listener(this);
        new PlayerDeathEvent_Listener(this);
        new PlayerLeashEntityEvent_Listener(this);
        new PotionSplashEvent_Listener(this);
        new ProjectileLaunchEvent_Listener(this);
        new SheepRegrowWoolEvent_Listener(this);
        new SlimeSplitEvent_Listener(this);
        new HangingBreakByEntityEvent_Listener(this);
        new HangingBreakEvent_Listener(this);
        new BrewEvent_Listener(this);
        new CraftItemEvent_Listener(this);
        new FurnaceBurnEvent_Listener(this);
        new FurnaceSmeltEvent_Listener(this);
        new InventoryClickEvent_Listener(this);
        new InventoryDragEvent_Listener(this);
        new InventoryMoveItemEvent_Listener(this);
        new PlayerAchievementAwardedEvent_Listener(this);
        new PlayerBedEnterEvent_Listener(this);
        new PlayerBucketEmptyEvent_Listener(this);
        new PlayerBucketFillEvent_Listener(this);
        new PlayerDropItemEvent_Listener(this);
        new PlayerEditBookEvent_Listener(this);
        new PlayerFishEvent_Listener(this);
        new PlayerInteractEntityEvent_Listener(this);
        new PlayerInteractEvent_Listener(this);
        new PlayerItemConsumeEvent_Listener(this);
        new PlayerKickEvent_Listener(this);
        new PlayerCommandPreprocessEvent_Listener(this);
        new PlayerPickupItemEvent_Listener(this);
        new PlayerPortalEvent_Listener(this);
        new PlayerShearEntityEvent_Listener(this);
        new VehicleEnterEvent_Listener(this);
        new WeatherChangeEvent_Listener(this);
        new CreatureSpawnEvent_Listener(this);

        new DoubleJump(this);
        new JumpPlates(this);

        new Navigator(this);
        new Flugstab(this);
        new Hider(this);
        new Schild(this);
        new LobbySwitcher(this);

        this.getCommand("gamemode").setExecutor(new GameMode(this));
        this.getCommand("set").setExecutor(new Set(this));
        this.getCommand("spawn").setExecutor(new Spawn(this));
        this.getCommand("teleport").setExecutor(new Teleport(this));

        Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
    }

    @Override
    public void onDisable() {
        this.getLogger().info(cmdprefix + "Plugin deaktiviert.");
    }

}
