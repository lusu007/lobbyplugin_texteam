package me.lusu007.lobbysystem.Methoden;

import com.connorlinfoot.titleapi.TitleAPI;
import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.entity.Player;

/**
 * Created by Lukas on 27.01.2015.
 */
public class TitleMethoden {

    private static Main plugin;

    public TitleMethoden(Main main) {
        this.plugin = main;
    }

    public static void joinMessage(Player p) {
        TitleAPI.sendTitle(p, 20*2, 20*4, 20*2, "§6Herzlich Willkommen §3" + p.getName(), "§6auf dem BlockBreaker.net Servernetzwerk!");
    }

    public void joinNewbieMessage(Player p) {
        TitleAPI.sendTitle(p, 20*2, 20*4, 20*2, "§6Herzlich Willkommen §3" + p.getName(), "§6auf dem BlockBreaker.net Servernetzwerk!");

        p.sendMessage("§6Du bist das erste Mal gejoint!");
        p.sendMessage("§6Teleportiere...");

        // TODO: Tutorial für Newbies. Messages ausgeben.
    }

}
