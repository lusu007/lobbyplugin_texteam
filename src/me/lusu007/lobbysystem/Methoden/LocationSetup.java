package me.lusu007.lobbysystem.Methoden;

import org.bukkit.entity.Player;

/**
 * Created by Lukas on 31.01.2015.
 */
public class LocationSetup {

    public static void sendSetupMessage(Player p) {
        p.sendMessage("§3===========[§6System§3]===========");
        p.sendMessage("§3/set sg - §6Setzt den SurvivalGames");
        p.sendMessage("§3/set tf - §6Setzt TeamForce");
        p.sendMessage("§3/set rpg - §6Setzt den RPG-Modus");
        p.sendMessage("§3/set paintball - §6Setzt Paintball");
        p.sendMessage("§3/set bedwars - §6Setzt Bedwars");
        p.sendMessage("§3/set skywars - §6Setzt Skywars");
        p.sendMessage("§3/set spawn - §6Setzt den Spawn");
        p.sendMessage("§3/set newbie - §6Setzt den Newbie-Spawn");
        p.sendMessage("§3/set premium - §6Setzt Premium-Bereich");
        p.sendMessage("§3/set island - §6Setzt die Insel");
        p.sendMessage("§3==============================");
    }
}
