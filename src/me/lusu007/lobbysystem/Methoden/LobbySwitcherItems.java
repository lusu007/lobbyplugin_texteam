package me.lusu007.lobbysystem.Methoden;

import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Created by Lukas on 09.02.2015.
 */
public class LobbySwitcherItems {

    private final Main plugin;

    public LobbySwitcherItems(Main main) {
        this.plugin = main;
    }

    public static void loadLobbySwitcherInv(Player p) {

        Inventory inv = Bukkit.createInventory(null, 9, "§6Lobbyswitcher");

        //=====================================================

        ItemStack lobby1 = new ItemStack(Material.SUGAR);
        ItemMeta lobby1meta = lobby1.getItemMeta();
        lobby1meta.setDisplayName("§aLobby 1");
        lobby1.setItemMeta(lobby1meta);

        //=====================================================

        ItemStack lobby2 = new ItemStack(Material.SUGAR);
        ItemMeta lobby2meta = lobby2.getItemMeta();
        lobby2meta.setDisplayName("§aLobby 2");
        lobby2.setItemMeta(lobby2meta);

        //=====================================================

        ItemStack silent = new ItemStack(Material.TNT);
        ItemMeta silentmeta = silent.getItemMeta();
        silentmeta.setDisplayName("§5SilentLobby");
        silent.setItemMeta(silentmeta);

        //=====================================================

        ItemStack premiumlobby = new ItemStack(Material.GOLDEN_APPLE);
        ItemMeta premiumlobbymeta = premiumlobby.getItemMeta();
        premiumlobbymeta.setDisplayName("§6PremiumLobby");
        premiumlobby.setItemMeta(premiumlobbymeta);

        //=====================================================

        ItemStack glass = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte)15);
        ItemMeta glassmeta = glass.getItemMeta();
        glassmeta.setDisplayName(" ");
        glass.setItemMeta(glassmeta);

        //=====================================================

        inv.setItem(0, glass);
        inv.setItem(1, glass);
        inv.setItem(2, glass);
        inv.setItem(3, glass);
        inv.setItem(4, glass);
        inv.setItem(5, glass);
        inv.setItem(6, glass);
        inv.setItem(7, glass);
        inv.setItem(8, glass);

        inv.setItem(0, lobby1);
        inv.setItem(1, lobby2);

        if(p.hasPermission("server.lobby.silent")) {
            inv.setItem(7, silent);
        }

        if(p.hasPermission("server.lobby.premium")) {
            inv.setItem(8, premiumlobby);
        }

        p.openInventory(inv);
        p.playSound(p.getLocation(), Sound.CHEST_OPEN, 1.0F, 1.0F);

    }
}
