package me.lusu007.lobbysystem.Methoden;

import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Created by Lukas on 30.01.2015.
 */
public class NavigatorItem {

    private final Main plugin;

    public NavigatorItem(Main main) {
        this.plugin = main;
    }

    public static void loadNavigatorInv(Player p) {

        Inventory inv = Bukkit.createInventory(null, 27, "§eWähle deinen Spielmodus");

        //=====================================================

        ItemStack SG = new ItemStack(Material.IRON_SWORD);
        ItemMeta SGmeta = SG.getItemMeta();
        SGmeta.setDisplayName("§aSurvivalGames");
        SG.setItemMeta(SGmeta);

        //=====================================================

        ItemStack tf = new ItemStack(Material.GOLD_HELMET);
        ItemMeta tfmeta = tf.getItemMeta();
        tfmeta.setDisplayName("§1Team-Force");
        tf.setItemMeta(tfmeta);

        //=====================================================

        ItemStack rpg = new ItemStack(Material.MAP);
        ItemMeta rpgmeta = rpg.getItemMeta();
        rpgmeta.setDisplayName("§cRPG");
        rpg.setItemMeta(rpgmeta);

        //=====================================================

        ItemStack paintball = new ItemStack(Material.SNOW_BALL);
        ItemMeta paintballmeta = paintball.getItemMeta();
        paintballmeta.setDisplayName("§5Paintball");
        paintball.setItemMeta(paintballmeta);

        //=====================================================

        ItemStack bedwars = new ItemStack(Material.BED);
        ItemMeta bedwarsmeta = bedwars.getItemMeta();
        bedwarsmeta.setDisplayName("§eBedwars");
        bedwars.setItemMeta(bedwarsmeta);

        //=====================================================

        ItemStack skywars = new ItemStack(Material.SNOW_BLOCK);
        ItemMeta skywarsmeta = skywars.getItemMeta();
        skywarsmeta.setDisplayName("§bSkyWars");
        skywars.setItemMeta(skywarsmeta);

        //=====================================================

        ItemStack spawn = new ItemStack(Material.EMERALD);
        ItemMeta spawnmeta = spawn.getItemMeta();
        spawnmeta.setDisplayName("§2Spawn");
        spawn.setItemMeta(spawnmeta);

        //=====================================================

        ItemStack newbie = new ItemStack(Material.SPONGE);
        ItemMeta newbiemeta = newbie.getItemMeta();
        newbiemeta.setDisplayName("§3Newbie-Spawn");
        newbie.setItemMeta(newbiemeta);

        //=====================================================

        ItemStack PREMIUM = new ItemStack(Material.GOLDEN_APPLE, 1, (short)1);
        ItemMeta PREMIUMmeta = PREMIUM.getItemMeta();
        PREMIUMmeta.setDisplayName("§6Premium Bereich");
        PREMIUM.setItemMeta(PREMIUMmeta);

        //=====================================================

        ItemStack comimgsoon = new ItemStack(Material.BOOK_AND_QUILL);
        ItemMeta comimgsoonmeta = comimgsoon.getItemMeta();
        comimgsoonmeta.setDisplayName("§fComing Soon");
        comimgsoon.setItemMeta(comimgsoonmeta);

        //=====================================================

        ItemStack glass = new ItemStack(Material.STAINED_GLASS_PANE, 1, (byte)15);
        ItemMeta glassmeta = glass.getItemMeta();
        glassmeta.setDisplayName(" ");
        glass.setItemMeta(glassmeta);

        //=====================================================

        inv.setItem(0, glass);
        inv.setItem(1, glass);
        inv.setItem(2, glass);
        inv.setItem(3, glass);
        inv.setItem(4, glass);
        inv.setItem(5, glass);
        inv.setItem(6, glass);
        inv.setItem(7, glass);
        inv.setItem(8, glass);
        inv.setItem(9, glass);
        inv.setItem(10, glass);
        inv.setItem(11, glass);
        inv.setItem(12, glass);
        inv.setItem(13, glass);
        inv.setItem(14, glass);
        inv.setItem(15, glass);
        inv.setItem(16, glass);
        inv.setItem(17, glass);
        inv.setItem(18, glass);
        inv.setItem(19, glass);
        inv.setItem(20, glass);
        inv.setItem(21, glass);
        inv.setItem(22, glass);
        inv.setItem(23, glass);
        inv.setItem(24, glass);
        inv.setItem(25, glass);
        inv.setItem(26, glass);

        inv.setItem(3, newbie);
        inv.setItem(4, spawn);
        inv.setItem(5, PREMIUM);
        inv.setItem(9, comimgsoon);
        inv.setItem(10, tf);
        inv.setItem(11, SG);
        inv.setItem(15, bedwars);
        inv.setItem(16, paintball);
        inv.setItem(17, skywars);
        inv.setItem(22, rpg);

        p.openInventory(inv);
        p.playSound(p.getLocation(), Sound.CHEST_OPEN, 1.0F, 1.0F);

    }
}