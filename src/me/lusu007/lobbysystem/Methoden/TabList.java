package me.lusu007.lobbysystem.Methoden;

import com.connorlinfoot.titleapi.TitleAPI;
import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created by Lukas on 27.01.2015.
 */
public class TabList {

    private static Main plugin;

    public TabList(Main main) {
        this.plugin = main;
    }


    public static void loadLobbyTablist(Player p) {
        TitleAPI.sendTabTitle(p, "§6BlockBreaker.net Servernetzwerk", "§6Spieler: §3 1§6/§31");
    }

    public static void clearLobbyTabList(Player p) {
        TitleAPI.sendTabTitle(p, null, null);
    }
}
