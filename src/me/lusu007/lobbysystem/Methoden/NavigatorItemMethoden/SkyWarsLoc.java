package me.lusu007.lobbysystem.Methoden.NavigatorItemMethoden;

import de.slikey.effectlib.effect.HelixEffect;
import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;

/**
 * Created by Lukas on 30.01.2015.
 */
public class SkyWarsLoc {

    private static Main plugin;

    public SkyWarsLoc(Main main) {
        this.plugin = main;
    }

    public static void setskywarsloc(Player p) {

        File file = new File("/plugins/LobbySystem", "skywars.yml");
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

        Location loc = p.getLocation();

        String world = p.getWorld().getName();
        double x = loc.getX();
        double y = loc.getY();
        double z = loc.getZ();
        double yaw = loc.getYaw();
        double pitch = loc.getPitch();

        cfg.set("skywars.spawn.world", world);
        cfg.set("skywars.spawn.x", x);
        cfg.set("skywars.spawn.y", y);
        cfg.set("skywars.spawn.z", z);
        cfg.set("skywars.spawn.yaw", yaw);
        cfg.set("skywars.spawn.pitch", pitch);

        try {
            cfg.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        p.sendMessage("§3Du hast erfolgreich den §6SkyWars §3Modus gesetzt!");
        p.playSound(p.getLocation(), Sound.LEVEL_UP, 1.0F, 1.0F);

        HelixEffect effect1 = new HelixEffect(plugin.em);
        effect1.setLocation(p.getLocation());
        effect1.start();

        HelixEffect effect2 = new HelixEffect(plugin.em);
        effect2.setLocation(p.getEyeLocation());
        effect2.start();


    }

    public static void teleportskywars(Player p) {

        File file = new File("/plugins/LobbySystem", "skywars.yml");
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

        String world = cfg.getString("skywars.spawn.world");
        double x = cfg.getDouble("skywars.spawn.x");
        double y = cfg.getDouble("skywars.spawn.y");
        double z = cfg.getDouble("skywars.spawn.z");
        double yaw = cfg.getDouble("skywars.spawn.yaw");
        double pitch = cfg.getDouble("skywars.spawn.pitch");

        Location loc = new Location(Bukkit.getWorld(world), x, y, z);
        loc.setYaw((float) yaw);
        loc.setPitch((float) pitch);

        p.teleport(loc);
    }

}
