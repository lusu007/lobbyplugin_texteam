package me.lusu007.lobbysystem.Methoden.NavigatorItemMethoden;

import de.slikey.effectlib.effect.HelixEffect;
import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;

/**
 * Created by Lukas on 30.01.2015.
 */
public class PaintballLoc {

    private static Main plugin;

    public PaintballLoc(Main main) {
        this.plugin = main;
    }

    public static void setpaintballloc(Player p) {

        File file = new File("/plugins/LobbySystem", "paintball.yml");
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

        Location loc = p.getLocation();

        String world = p.getWorld().getName();
        double x = loc.getX();
        double y = loc.getY();
        double z = loc.getZ();
        double yaw = loc.getYaw();
        double pitch = loc.getPitch();

        cfg.set("paintball.spawn.world", world);
        cfg.set("paintball.spawn.x", x);
        cfg.set("paintball.spawn.y", y);
        cfg.set("paintball.spawn.z", z);
        cfg.set("paintball.spawn.yaw", yaw);
        cfg.set("paintball.spawn.pitch", pitch);

        try {
            cfg.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        p.sendMessage("§3Du hast erfolgreich den §6Paintball §3Modus gesetzt!");
        p.playSound(p.getLocation(), Sound.LEVEL_UP, 1.0F, 1.0F);

        HelixEffect effect1 = new HelixEffect(plugin.em);
        effect1.setLocation(p.getLocation());
        effect1.start();

        HelixEffect effect2 = new HelixEffect(plugin.em);
        effect2.setLocation(p.getEyeLocation());
        effect2.start();


    }

    public static void teleportpaintball(Player p) {

        File file = new File("/plugins/LobbySystem", "paintball.yml");
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

        String world = cfg.getString("paintball.spawn.world");
        double x = cfg.getDouble("paintball.spawn.x");
        double y = cfg.getDouble("paintball.spawn.y");
        double z = cfg.getDouble("paintball.spawn.z");
        double yaw = cfg.getDouble("paintball.spawn.yaw");
        double pitch = cfg.getDouble("paintball.spawn.pitch");

        Location loc = new Location(Bukkit.getWorld(world), x, y, z);
        loc.setYaw((float) yaw);
        loc.setPitch((float) pitch);

        p.teleport(loc);
    }

}
