package me.lusu007.lobbysystem.Methoden.NavigatorItemMethoden;

import de.slikey.effectlib.effect.HelixEffect;
import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;

/**
 * Created by Lukas on 30.01.2015.
 */
public class TeamForceLoc {

    private static Main plugin;

    public TeamForceLoc(Main main) {
        this.plugin = main;
    }

    public static void settfloc(Player p) {

        File file = new File("/plugins/LobbySystem", "tf.yml");
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

        Location loc = p.getLocation();

        String world = p.getWorld().getName();
        double x = loc.getX();
        double y = loc.getY();
        double z = loc.getZ();
        double yaw = loc.getYaw();
        double pitch = loc.getPitch();

        cfg.set("tf.spawn.world", world);
        cfg.set("tf.spawn.x", x);
        cfg.set("tf.spawn.y", y);
        cfg.set("tf.spawn.z", z);
        cfg.set("tf.spawn.yaw", yaw);
        cfg.set("tf.spawn.pitch", pitch);

        try {
            cfg.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        p.sendMessage("§3Du hast erfolgreich den §6TeamForce §3Modus gesetzt!");
        p.playSound(p.getLocation(), Sound.LEVEL_UP, 1.0F, 1.0F);

        HelixEffect effect1 = new HelixEffect(plugin.em);
        effect1.setLocation(p.getLocation());
        effect1.start();

        HelixEffect effect2 = new HelixEffect(plugin.em);
        effect2.setLocation(p.getEyeLocation());
        effect2.start();


    }

    public static void teleporttf(Player p) {

        File file = new File("/plugins/LobbySystem", "tf.yml");
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

        String world = cfg.getString("tf.spawn.world");
        double x = cfg.getDouble("tf.spawn.x");
        double y = cfg.getDouble("tf.spawn.y");
        double z = cfg.getDouble("tf.spawn.z");
        double yaw = cfg.getDouble("tf.spawn.yaw");
        double pitch = cfg.getDouble("tf.spawn.pitch");

        Location loc = new Location(Bukkit.getWorld(world), x, y, z);
        loc.setYaw((float) yaw);
        loc.setPitch((float) pitch);

        p.teleport(loc);
    }
}
