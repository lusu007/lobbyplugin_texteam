package me.lusu007.lobbysystem.Methoden.NavigatorItemMethoden;

import de.slikey.effectlib.effect.HelixEffect;
import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;

/**
 * Created by Lukas on 30.01.2015.
 */
public class IslandLoc {

    private static Main plugin;

    public IslandLoc(Main main) {
        this.plugin = main;
    }

    public static void setislandloc(Player p) {

        File file = new File("/plugins/LobbySystem", "island.yml");
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

        Location loc = p.getLocation();

        String world = p.getWorld().getName();
        double x = loc.getX();
        double y = loc.getY();
        double z = loc.getZ();
        double yaw = loc.getYaw();
        double pitch = loc.getPitch();

        cfg.set("island.spawn.world", world);
        cfg.set("island.spawn.x", x);
        cfg.set("island.spawn.y", y);
        cfg.set("island.spawn.z", z);
        cfg.set("island.spawn.yaw", yaw);
        cfg.set("island.spawn.pitch", pitch);

        try {
            cfg.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        p.sendMessage("§3Du hast erfolgreich den §6Island §3Spawn gesetzt!");
        p.playSound(p.getLocation(), Sound.LEVEL_UP, 1.0F, 1.0F);

        HelixEffect effect1 = new HelixEffect(plugin.em);
        effect1.setLocation(p.getLocation());
        effect1.start();

        HelixEffect effect2 = new HelixEffect(plugin.em);
        effect2.setLocation(p.getEyeLocation());
        effect2.start();


    }

    public static void teleportisland(Player p) {

        File file = new File("/plugins/LobbySystem", "island.yml");
        FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);

        String world = cfg.getString("island.spawn.world");
        double x = cfg.getDouble("island.spawn.x");
        double y = cfg.getDouble("island.spawn.y");
        double z = cfg.getDouble("island.spawn.z");
        double yaw = cfg.getDouble("island.spawn.yaw");
        double pitch = cfg.getDouble("island.spawn.pitch");

        Location loc = new Location(Bukkit.getWorld(world), x, y, z);
        loc.setYaw((float) yaw);
        loc.setPitch((float) pitch);

        p.teleport(loc);
    }

}
