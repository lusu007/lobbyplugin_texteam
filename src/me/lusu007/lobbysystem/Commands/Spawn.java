package me.lusu007.lobbysystem.Commands;

import me.lusu007.lobbysystem.Main.Main;
import me.lusu007.lobbysystem.Methoden.NavigatorItemMethoden.SpawnLoc;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Lukas on 31.01.2015.
 */
public class Spawn implements CommandExecutor {

    private final Main plugin;

    public Spawn(Main main) {
        this.plugin = main;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if(sender instanceof Player) {
            Player p = (Player) sender;

            SpawnLoc.teleportspawn(p);
        }

        return true;
    }
}
