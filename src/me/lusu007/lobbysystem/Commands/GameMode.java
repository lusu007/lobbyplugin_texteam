package me.lusu007.lobbysystem.Commands;

import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Lukas on 09.01.2015.
 */
public class GameMode implements CommandExecutor {

    private final Main plugin;

    public GameMode(Main main) {
        this.plugin = main;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(sender instanceof Player) {
            Player p = (Player) sender;

            if(p.hasPermission("server.set.gamemode")) {
                if (args.length == 0) {

                    if (p.getGameMode() == org.bukkit.GameMode.CREATIVE) {
                        p.setGameMode(org.bukkit.GameMode.SURVIVAL);
                        p.sendMessage("§3Du bist im §e'Survival' §3Modus.");
                        return true;
                    }

                    if (p.getGameMode() == org.bukkit.GameMode.SURVIVAL) {
                        p.setGameMode(org.bukkit.GameMode.CREATIVE);
                        p.sendMessage("§3Du bist im §e'Creative' §3Modus.");
                        return true;
                    }

                    if (p.getGameMode() == org.bukkit.GameMode.ADVENTURE) {
                        p.setGameMode(org.bukkit.GameMode.SURVIVAL);
                        p.sendMessage("§3Du bist im §e'Survival' §3Modus.");
                        return true;
                    }

                } else
                    if (args.length == 1) {

                    if (args[0].equalsIgnoreCase("0")) {
                        p.setGameMode(org.bukkit.GameMode.SURVIVAL);
                        p.sendMessage("§3Du bist im §e'Survival' §3Modus.");
                        return true;
                    } else if (args[0].equalsIgnoreCase("1")) {
                        p.setGameMode(org.bukkit.GameMode.CREATIVE);
                        p.sendMessage("§3Du bist im §e'Creative' §3Modus.");
                        return true;
                    } else if (args[0].equalsIgnoreCase("2")) {
                        p.setGameMode(org.bukkit.GameMode.ADVENTURE);
                        p.sendMessage("§3Du bist im §e'Adventure' §3Modus.");
                        return true;
                    } else {
                            p.sendMessage("§3Du verwendest falsche §6Argumente!");
                            return true;
                    }

                } else
                    if(args.length == 2) {

                        Player target;

                        if(args[0].equalsIgnoreCase("0")) {
                            target = Bukkit.getPlayer(args[1]);

                            if(target != null) {
                                target.setGameMode(org.bukkit.GameMode.SURVIVAL);
                                p.sendMessage("§3Du hast §6" + target.getName() + " §3in den §e'Kreativ' §3Modus gesetzt.");
                                return true;
                            }
                        }

                        if(args[0].equalsIgnoreCase("1")) {
                            target = Bukkit.getPlayer(args[1]);

                            if(target != null) {
                                target.setGameMode(org.bukkit.GameMode.CREATIVE);
                                p.sendMessage("§3Du hast §6" + target.getName() + " §3in den §e'Kreativ' §3Modus gesetzt.");
                                return true;
                            }
                        }

                        if(args[0].equalsIgnoreCase("2")) {
                            target = Bukkit.getPlayer(args[1]);

                            if(target != null) {
                                target.setGameMode(org.bukkit.GameMode.ADVENTURE);
                                p.sendMessage("§3Du hast §6" + target.getName() + " §3in den §e'Adventure' §3Modus gesetzt.");
                                return true;
                            }
                        }
                    }

            }

        }

        return true;
    }
}
