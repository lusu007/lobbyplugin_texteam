package me.lusu007.lobbysystem.Commands;

import me.lusu007.lobbysystem.Main.Main;
import me.lusu007.lobbysystem.Methoden.LocationSetup;
import me.lusu007.lobbysystem.Methoden.NavigatorItemMethoden.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Lukas on 31.01.2015.
 */
public class Set implements CommandExecutor {

    private final Main plugin;

    public Set(Main main) {
        this.plugin = main;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if(sender instanceof Player) {

            Player p= (Player) sender;

            if(p.hasPermission("server.locations.set")) {

                if(args.length == 0) {
                    LocationSetup.sendSetupMessage(p);
                    return true;
                } else
                if(args.length == 1) {

                    if(args[0].equalsIgnoreCase("sg")) {
                        SGLoc.setsgloc(p);
                    } else if(args[0].equalsIgnoreCase("tf")) {
                        TeamForceLoc.settfloc(p);
                    } else if(args[0].equalsIgnoreCase("rpg")) {
                        RPGLoc.setrpgloc(p);
                    } else if(args[0].equalsIgnoreCase("paintball")) {
                        PaintballLoc.setpaintballloc(p);
                    } else if(args[0].equalsIgnoreCase("bedwars")) {
                        BedwarsLoc.setbedwarsloc(p);
                    } else if(args[0].equalsIgnoreCase("skywars")) {
                        SkyWarsLoc.setskywarsloc(p);
                    } else if(args[0].equalsIgnoreCase("spawn")) {
                        SpawnLoc.setspawnloc(p);
                    } else if(args[0].equalsIgnoreCase("newbie")) {
                        NewbieLoc.setnewbieloc(p);
                    } else if(args[0].equalsIgnoreCase("premium")) {
                        PremiumLoc.setpremiumloc(p);
                    } else if(args[0].equalsIgnoreCase("island")) {
                        IslandLoc.setislandloc(p);
                    } else {
                        p.sendMessage("§3Leider gibt es keine §6Befehle §3mit diesen §6Argumenten!");
                        return true;
                    }

                } else {
                    p.sendMessage("§3Leider gibt es keine §6Befehle §3mit diesen §6Argumenten!");
                    return true;
                }

            } else {
                p.sendMessage(plugin.noperm);
                return true;
            }

        } else {
            sender.sendMessage("Dir ist es leider nicht möglich Locations zu setzen. Diese Funktion können nur Spieler InGame benutzten!");
            return true;
        }

        return true;
    }
}
