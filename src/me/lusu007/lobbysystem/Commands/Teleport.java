package me.lusu007.lobbysystem.Commands;

import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Created by Lukas on 31.01.2015.
 */
public class Teleport implements CommandExecutor {

    private final Main plugin;

    public Teleport(Main main) {
        this.plugin = main;
    }


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if(sender instanceof Player) {

            Player p = (Player) sender;

            if(p.hasPermission("server.teleport")) {
                if(args.length == 1) {

                    Player teleportplayer = Bukkit.getPlayer(args[0]);


                    if(teleportplayer != null) {

                        Location teleportplayerloc = teleportplayer.getLocation();

                        p.teleport(teleportplayerloc);

                        p.sendMessage("§3Du hast dich zu §6" + teleportplayer.getDisplayName() + " §3teleportiert.");

                    } else {
                        p.sendMessage("§3Dieser Spieler §6" + args[0] + " §3ist nicht Online.");
                        return true;
                    }

                } else {
                    p.sendMessage("§3/teleport [Spieler]");
                    return true;
                }

            } else {
                p.sendMessage(plugin.noperm);
                return true;
            }

        } else {
            sender.sendMessage("Dir ist es nicht möglich Spieler zu teleportieren. Diese Funktion können nur Spieler InGame benutzten!");
            return true;
        }

        return true;
    }
}
