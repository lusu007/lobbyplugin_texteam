package me.lusu007.lobbysystem.ItemManager;

import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

/**
 * Created by Lukas on 08.01.2015.
 */
public class Schild implements Listener {

    private final Main plugin;

    public Schild(Main main) {
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();

        if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            try {

                if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§5Schutzschild")) {

                    p.sendMessage("§6Du kannst das §3Schutzschild §6nicht in der Silent Lobby verwenden.");
                }
            } catch (Exception ex) {
            }
        }
    }
}
