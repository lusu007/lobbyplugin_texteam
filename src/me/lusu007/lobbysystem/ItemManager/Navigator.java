package me.lusu007.lobbysystem.ItemManager;

/**
 * Created by Lukas on 08.01.2015.
 */

import me.lusu007.lobbysystem.Main.Main;
import me.lusu007.lobbysystem.Methoden.NavigatorItem;
import me.lusu007.lobbysystem.Methoden.NavigatorItemMethoden.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class Navigator implements Listener {


    private final Main plugin;

    public Navigator(Main main) {
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }

    @EventHandler
    public void onNavigatorInteract(PlayerInteractEvent e) {

        if(e.getAction() == Action.RIGHT_CLICK_BLOCK | e.getAction() == Action.RIGHT_CLICK_AIR) {
            try {
                if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Navigator")) {
                    Player p = e.getPlayer();

                    NavigatorItem.loadNavigatorInv(p);
                }
            } catch(Exception ex) {

            }
        }
    }

    @EventHandler
    public void onNavigatorInvInteract(InventoryClickEvent e) {

        Player p = (Player) e.getWhoClicked();

        if (e.getInventory().getName().equalsIgnoreCase("§eWähle deinen Spielmodus")) {
            try {
                if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aSurvivalGames")) {
                    SGLoc.teleportsg(p);
                }
            } catch(Exception ex) {
            }

            try {
                if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§1Team-Force")) {
                    TeamForceLoc.teleporttf(p);
                }
            } catch(Exception ex) {
            }

            try {
                if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cRPG")) {
                    RPGLoc.teleportrpg(p);
                }
            } catch(Exception ex) {
            }

            try {
                if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§5Paintball")) {
                    PaintballLoc.teleportpaintball(p);
                }
            } catch(Exception ex) {
            }

            try {
                if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§eBedwars")) {
                    BedwarsLoc.teleportbedwars(p);
                }
            } catch(Exception ex) {
            }

            try {
                if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§bSkyWars")) {
                    SkyWarsLoc.teleportskywars(p);
                }
            } catch(Exception ex) {
            }

            try {
                if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§2Spawn")) {
                    SpawnLoc.teleportspawn(p);
                }
            } catch(Exception ex) {
            }

            try {
                if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§3Newbie-Spawn")) {
                    NewbieLoc.teleportnewbie(p);
                }
            } catch(Exception ex) {
            }

            try {
                if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Premium Bereich")) {
                    PremiumLoc.teleportpremium(p);
                }
            } catch(Exception ex) {
            }
        }
    }
}
