package me.lusu007.lobbysystem.ItemManager;

import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;
import org.bukkit.event.Listener;

/**
 * Created by Lukas on 08.01.2015.
 */
public class Flugstab implements Listener {

    private final Main plugin;

    public Flugstab(Main main) {
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }


    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        if(e.getPlayer().getGameMode() == GameMode.SURVIVAL | e.getPlayer().getGameMode() == GameMode.ADVENTURE) {
            if(e.getAction() == Action.RIGHT_CLICK_BLOCK | e.getAction() == Action.RIGHT_CLICK_AIR) {
                try {
                    if (e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§bFlugstab")) {
                        Player p = e.getPlayer();

                        if (p.getGameMode() != GameMode.CREATIVE & plugin.flying.contains(p.getName())) {

                            Vector v = p.getLocation().getDirection().multiply(1.5D * 1.5D).setY(1.0D);
                            p.setVelocity(v);

                            for (Player pl : Bukkit.getOnlinePlayers()) {
                                pl.playEffect(p.getLocation(), Effect.MOBSPAWNER_FLAMES, 2004);
                            }

                            p.playSound(p.getLocation(), Sound.ENDERDRAGON_WINGS, 1.0F, 1.0F);

                        }
                    }
                } catch (Exception ex) {
                }
            }
        }

        if(e.getPlayer().getGameMode() == GameMode.SURVIVAL | e.getPlayer().getGameMode() == GameMode.ADVENTURE) {
            if (e.getAction() == Action.LEFT_CLICK_BLOCK | e.getAction() == Action.LEFT_CLICK_AIR) {
                try {
                    if (e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§bFlugstab")) {

                        final Player p = e.getPlayer();

                        if (!plugin.flying.contains(p.getName())) {

                            p.setGameMode(GameMode.ADVENTURE);


                            plugin.flying.add(p.getName());

                            p.sendMessage("§3Du hast den §bFlugstab §3aktiviert.");

                            p.setAllowFlight(true);

                            plugin.jumper.put(p.getName(), new BukkitRunnable() {
                                @Override
                                public void run() {
                                    p.setFlying(false);
                                }
                            });
                            plugin.jumper.get(p.getName()).runTaskTimer(plugin, 0, 1);

                            return;
                        }

                        if (plugin.flying.contains(p.getName())) {
                            plugin.flying.remove(p.getName());

                            p.setFlying(false);
                            p.setAllowFlight(false);

                            p.sendMessage("§3Du hast den §bFlugstab §3deaktiviert.");

                            plugin.jumper.get(p.getName()).cancel();

                            plugin.jumper.get(p.getName());
                            plugin.jumper.remove(p.getName());


                            return;
                        }
                    }
                } catch (Exception ex) {
                }
            }
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();

        if (plugin.flying.contains(p.getName())) {
            plugin.flying.remove(p.getName());

            p.setFlying(false);
            p.setAllowFlight(false);

            plugin.jumper.get(p.getName()).cancel();

            plugin.jumper.get(p.getName());
            plugin.jumper.remove(p.getName());
        }
    }
}
