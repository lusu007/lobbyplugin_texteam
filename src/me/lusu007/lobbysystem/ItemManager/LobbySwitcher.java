package me.lusu007.lobbysystem.ItemManager;

import me.lusu007.lobbysystem.Main.Main;
import me.lusu007.lobbysystem.Methoden.LobbySwitcherItems;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;

/**
 * Created by Lukas on 08.01.2015.
 */
public class LobbySwitcher implements Listener {

    private final Main plugin;

    public LobbySwitcher(Main main) {
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }

    @EventHandler
    public void onNavigatorInteract(PlayerInteractEvent e) {

        if(e.getAction() == Action.RIGHT_CLICK_BLOCK | e.getAction() == Action.RIGHT_CLICK_AIR) {
            try {
                if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§2Lobbyswitcher")) {

                    Player p = e.getPlayer();

                    LobbySwitcherItems.loadLobbySwitcherInv(p);

                }
            } catch(Exception ex) {

            }
        }
    }

    @EventHandler
    public void onNavigatorInvInteract(InventoryClickEvent e) {

        Player p = (Player) e.getWhoClicked();

        if (e.getInventory().getName().equalsIgnoreCase("§6Lobbyswitcher")) {
            try {
                if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aLobby 1")) {
                    joinServer(p, "lobby1");
                    e.setCancelled(true);
                }
            } catch(Exception ex) {
            }

            try {
                if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aLobby 2")) {
                    joinServer(p, "lobby2");
                    e.setCancelled(true);
                }
            } catch(Exception ex) {
            }

            try {
                if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6PremiumLobby")) {
                    joinServer(p, "premium");
                    e.setCancelled(true);
                }
            } catch(Exception ex) {
            }

            try {
                if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§5SilentLobby")) {
                    joinServer(p, "silent");
                    e.setCancelled(true);
                }
            } catch(Exception ex) {
            }
        }
    }


    private void joinServer(Player p, String servername) {

        ByteArrayOutputStream b = new ByteArrayOutputStream();
        DataOutputStream out = new DataOutputStream(b);

        try {
            out.writeUTF("Connect");
            out.writeUTF(servername);
        } catch(Exception ex) {
        }

        p.sendPluginMessage(plugin, "BungeeCord", b.toByteArray());
    }
}
