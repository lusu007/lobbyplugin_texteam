package me.lusu007.lobbysystem.ItemManager;

import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by Lukas on 08.01.2015.
 */
public class Hider implements Listener {

    private final Main plugin;

    public Hider(Main main) {
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {

        Player p = e.getPlayer();

        if(e.getAction() == Action.RIGHT_CLICK_AIR | e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            try {

                if(e.getItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cSpieler verstecken")) {
                    p.sendMessage("§6Der §3Hider §6kann in der Silent Lobby nicht verwendet werden.");
                }

            } catch(Exception ex) {
            }
        }

    }


    @EventHandler
    public void onInvInteract(InventoryClickEvent e) {

        Player p = (Player) e.getWhoClicked();

        if(e.getInventory().getName().equalsIgnoreCase("§6Spieler verstecken")) {

            try {
                if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(" ")) {
                    e.setCancelled(true);
                }
            } catch(Exception ex) {
            }

            try {
                if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aAlle Spieler anzeigen")) {
                    e.setCancelled(true);

                    if(plugin.hidden1.contains(p.getName())) {
                        plugin.hidden1.remove(p.getName());
                    }

                    if(plugin.hidden2.contains(p.getName())) {
                        plugin.hidden2.remove(p.getName());
                    }

                    for(Player players : Bukkit.getOnlinePlayers()) {
                        p.showPlayer(players);
                        p.playSound(p.getLocation(), Sound.LEVEL_UP, 1.0F, 1.0F);
                    }
                    //Bukkit.getOnlinePlayers().forEach(player -> {
                      //  p.showPlayer(player);
                        //p.playSound(p.getLocation(), Sound.LEVEL_UP, 1.0F, 1.0F);
                    //});

                }

                p.closeInventory();
            } catch(Exception ex) {
            }

            try {
                if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§5Nur Team-Mitglieder und Premium anzeigen")) {
                    e.setCancelled(true);

                    if(plugin.hidden1.contains(p.getName())) {
                        plugin.hidden1.remove(p.getName());
                    }

                    if(!plugin.hidden2.contains(p.getName())) {
                        plugin.hidden2.add(p.getName());
                    }

                    for(Player players : Bukkit.getOnlinePlayers()) {
                        if(players.hasPermission("server.visible")) {
                            p.showPlayer(players);
                        } else {
                            p.hidePlayer(players);
                            p.playSound(p.getLocation(), Sound.LEVEL_UP, 1.0F, 1.0F);
                        }
                    }
                }
                p.closeInventory();
            } catch(Exception ex) {
            }

            try {
                if(e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cAlle Spieler ausblenden")) {
                    e.setCancelled(true);

                    if(plugin.hidden1.contains(p.getName())) {
                        plugin.hidden1.remove(p.getName());
                    }

                    if(!plugin.hidden2.contains(p.getName())) {
                        plugin.hidden2.remove(p.getName());
                    }

                    Bukkit.getOnlinePlayers().forEach(player -> {
                        p.hidePlayer(player);
                        p.playSound(p.getLocation(), Sound.LEVEL_UP, 1.0F, 1.0F);
                    });
                }
                p.closeInventory();
            } catch(Exception ex) {
            }
        }

    }


    @EventHandler
    public void onJoin(PlayerJoinEvent e) {

        Player p = e.getPlayer();

        for(Player players : Bukkit.getOnlinePlayers()) {

            if(plugin.hidden1.contains(players.getName())) {
                players.hidePlayer(p);
                return;
            }

            if(plugin.hidden2.contains(players.getName())) {

                if(p.hasPermission("server.visible")) {
                    players.showPlayer(p);
                } else {
                    players.hidePlayer(p);
                }
                return;
            }

            if(!plugin.hidden1.contains(players.getName())  & plugin.hidden2.contains(players.getName())) {
                players.showPlayer(p);
            }

        }
    }
}
