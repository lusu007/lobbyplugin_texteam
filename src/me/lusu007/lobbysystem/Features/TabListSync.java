package me.lusu007.lobbysystem.Features;

import me.lusu007.lobbysystem.Methoden.TabList;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created by Lukas on 30.01.2015.
 */
public class TabListSync implements Runnable {

    @Override
    public void run() {
        for(Player players : Bukkit.getOnlinePlayers()) {
            TabList.loadLobbyTablist(players);
        }
    }

}
