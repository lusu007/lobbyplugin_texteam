package me.lusu007.lobbysystem.Features;

import me.lusu007.lobbysystem.Methoden.Time;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created by Lukas on 27.01.2015.
 */
public class TimeSet implements Runnable {

    @Override
    public void run() {
        for(Player players : Bukkit.getOnlinePlayers()) {
            Time.settime(players);
        }
    }
}
