package me.lusu007.lobbysystem.Features;

import de.slikey.effectlib.effect.WarpEffect;
import de.slikey.effectlib.util.ParticleEffect;
import me.lusu007.lobbysystem.Main.Main;
import me.lusu007.lobbysystem.Methoden.InventoryClear;
import me.lusu007.lobbysystem.Methoden.NavigatorItemMethoden.IslandLoc;
import me.lusu007.lobbysystem.Methoden.NavigatorItemMethoden.SGLoc;
import me.lusu007.lobbysystem.Methoden.NavigatorItemMethoden.SkyWarsLoc;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;
import org.bukkit.util.Vector;

/**
 * Created by Lukas on 27.01.2015.
 */
public class JumpPlates implements Listener {

    private final Main plugin;

    public JumpPlates(Main main) {
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }


    @EventHandler
    public void onMove(PlayerMoveEvent e) {

        Player p = e.getPlayer();

        if(p.getLocation().getBlock().getType() == Material.GOLD_PLATE) {
            if(p.getLocation().subtract(0.0D, 1.0D, 0.0D).getBlock().getType() == Material.COBBLESTONE) {

                    Vector v = p.getLocation().getDirection().multiply(2.0D).setY(1.0D);
                    p.setVelocity(v);

                    ParticleEffect.PORTAL.display(p.getLocation(), 1, 1, 1, 1, 1, 50);
                    ParticleEffect.PORTAL.display(p.getEyeLocation(), 1, 1, 1, 1, 1, 50);

                    p.playSound(p.getLocation(), Sound.ENDERDRAGON_WINGS, 1.0F, 1.0F);
            }
        }

        if(p.getLocation().getBlock().getType() == Material.GOLD_PLATE) {
            if(p.getLocation().subtract(0.0D, 1.0D, 0.0D).getBlock().getType() == Material.STONE) {

                Vector v = p.getLocation().getDirection().multiply(8.0D).setY(1.0D);
                p.setVelocity(v);

                ParticleEffect.PORTAL.display(p.getLocation(), 1, 1, 1, 1, 1, 50);
                ParticleEffect.PORTAL.display(p.getEyeLocation(), 1, 1, 1, 1, 1, 50);

                p.playSound(p.getLocation(), Sound.ENDERDRAGON_WINGS, 1.0F, 1.0F);
            }
        }

        if(p.getLocation().getBlock().getType() == Material.GOLD_PLATE) {
            if(p.getLocation().subtract(0.0D, 1.0D, 0.0D).getBlock().getType() == Material.IRON_BLOCK) {

                Vector v = p.getLocation().getDirection().multiply(20.0D).setY(1.0D);
                p.setVelocity(v);

                ParticleEffect.PORTAL.display(p.getLocation(), 1, 1, 1, 1, 1, 50);
                ParticleEffect.PORTAL.display(p.getEyeLocation(), 1, 1, 1, 1, 1, 50);

                p.playSound(p.getLocation(), Sound.ENDERDRAGON_WINGS, 1.0F, 1.0F);
            }
        }

        if(p.getLocation().getBlock().getType() == Material.GOLD_PLATE) {
            if(p.getLocation().subtract(0.0D, 1.0D, 0.0D).getBlock().getType() == Material.GOLD_BLOCK) {

                WarpEffect we = new WarpEffect(plugin.em);
                we.radius = 1;
                we.grow = 0.5F;
                we.rings = 5;
                we.autoOrient = true;
                we.setLocation(p.getLocation());
                we.start();

                p.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 18000, 2), true);

                Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                    @Override
                    public void run() {
                        p.playSound(p.getLocation(), Sound.NOTE_PIANO, 10F, 1.42F);
                        InventoryClear.clearEffects(p);
                        SkyWarsLoc.teleportskywars(p);
                    }
                }, 20*3);
            }
        }

        if(p.getLocation().getBlock().getType() == Material.GOLD_PLATE) {
            if(p.getLocation().subtract(0.0D, 1.0D, 0.0D).getBlock().getType() == Material.WOOD_DOUBLE_STEP) {

                WarpEffect we = new WarpEffect(plugin.em);
                we.radius = 1;
                we.grow = 0.5F;
                we.rings = 5;
                we.autoOrient = true;
                we.setLocation(p.getLocation());
                we.start();

                Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
                    @Override
                    public void run() {
                        p.playSound(p.getLocation(), Sound.NOTE_PIANO, 10F, 1.42F);
                        IslandLoc.teleportisland(p);
                    }
                }, 20*3);
            }
        }
    }
}

