package me.lusu007.lobbysystem.Listener;

import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 * Created by Lukas on 05.01.2015.
 */
public class EntityPortalEvent_Listener implements Listener {

    private final Main plugin;

    public EntityPortalEvent_Listener(Main main) {
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }



    @EventHandler
    public void onEntityPortal(org.bukkit.event.entity.EntityPortalEvent e) {
        e.setCancelled(true);
    }
}
