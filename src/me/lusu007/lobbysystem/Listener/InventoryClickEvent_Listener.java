package me.lusu007.lobbysystem.Listener;

import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 * Created by Lukas on 05.01.2015.
 */
public class InventoryClickEvent_Listener implements Listener {

    private final Main plugin;

    public InventoryClickEvent_Listener(Main main) {
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }


    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        try {
            if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Navigator")) {
                e.setCancelled(true);
            }

            if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cSpieler verstecken")) {
                e.setCancelled(true);
            }

            if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§5Schutzschild")) {
                e.setCancelled(true);
            }

            if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§eAuto-Nicker")) {
                e.setCancelled(true);
            }

            if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§bFlugstab")) {
                e.setCancelled(true);
            }

            if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§2Lobbyswitcher")) {
                e.setCancelled(true);
            }

            if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Erweiterte Auswahl")) {
                e.setCancelled(true);
            }

            if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Freunde")) {
                e.setCancelled(true);
            }

            if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§bSpawn")) {
                e.setCancelled(true);
            }

            if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§aSurvivalGames")) {
                e.setCancelled(true);
            }

            if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§1Team-Force")) {
                e.setCancelled(true);
            }

            if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cRPG")) {
                e.setCancelled(true);
            }

            if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§5Paintball")) {
                e.setCancelled(true);
            }

            if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§eBedwars")) {
                e.setCancelled(true);
            }

            if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§bSkyWars")) {
                e.setCancelled(true);
            }

            if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§2Spawn")) {
                e.setCancelled(true);
            }

            if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§3Newbie-Spawn")) {
                e.setCancelled(true);
            }

            if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§6Premium Bereich")) {
                e.setCancelled(true);
            }

            if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§fComing Soon")) {
                e.setCancelled(true);
            }

            if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase(" ")) {
                e.setCancelled(true);
            }
        } catch (Exception ex) {

        }
    }
}
