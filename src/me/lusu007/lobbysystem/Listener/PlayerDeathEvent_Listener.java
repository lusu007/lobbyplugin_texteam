package me.lusu007.lobbysystem.Listener;

import me.lusu007.lobbysystem.Main.Main;
import me.lusu007.lobbysystem.Methoden.NavigatorItemMethoden.SpawnLoc;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

/**
 * Created by Lukas on 05.01.2015.
 */
public class PlayerDeathEvent_Listener implements Listener {

    private final Main plugin;

    public PlayerDeathEvent_Listener(Main main) {
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }


    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent e) {
        Player p = e.getEntity();

        e.setDeathMessage(null);

        SpawnLoc.teleportspawn(p);
    }
}
