package me.lusu007.lobbysystem.Listener;

import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.FurnaceSmeltEvent;

/**
 * Created by Lukas on 05.01.2015.
 */
public class FurnaceSmeltEvent_Listener implements Listener {

    private final Main plugin;

    public FurnaceSmeltEvent_Listener(Main main) {
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }


    @EventHandler
    public void onFurnaceSmelt(FurnaceSmeltEvent e) {
        e.setCancelled(true);
    }
}
