package me.lusu007.lobbysystem.Listener;

import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBedEnterEvent;

/**
 * Created by Lukas on 05.01.2015.
 */
public class PlayerBedEnterEvent_Listener implements Listener {

    private final Main plugin;

    public PlayerBedEnterEvent_Listener(Main main) {
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }


    @EventHandler
    public void onPlayerBedEnter(PlayerBedEnterEvent e) {
        e.setCancelled(true);
    }
}
