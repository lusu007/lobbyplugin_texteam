package me.lusu007.lobbysystem.Listener;

import me.lusu007.lobbysystem.API.MySQL.MySQL;
import me.lusu007.lobbysystem.Features.TabListSync;
import me.lusu007.lobbysystem.Features.TimeSet;
import me.lusu007.lobbysystem.Main.Main;
import me.lusu007.lobbysystem.Methoden.InventoryClear;
import me.lusu007.lobbysystem.Methoden.InventoryItems;
import me.lusu007.lobbysystem.Methoden.NavigatorItemMethoden.NewbieLoc;
import me.lusu007.lobbysystem.Methoden.NavigatorItemMethoden.SpawnLoc;
import me.lusu007.lobbysystem.Methoden.TabList;
import me.lusu007.lobbysystem.Methoden.TitleMethoden;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * Created by Lukas on 04.01.2015.
 */
public class PlayerJoinEvent_Listener implements Listener {

    private final Main plugin;

    public PlayerJoinEvent_Listener(Main main) {
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }


    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        final Player p = e.getPlayer();

        InventoryClear.clearInv(p);
        InventoryClear.removeBoots(p);
        InventoryClear.clearEffects(p);
        InventoryClear.joinPlayer(p);

        plugin.onlineplayer.add(p.getName());

        //TabList.clearLobbyTabList(p);
        //TabList.loadLobbyTablist(p);

        InventoryItems.getInGameItems(p);

        e.setJoinMessage(null);

        // Startet Zeizgleichsetzung
        if (Bukkit.getOnlinePlayers().size() == 1) {
            plugin.timecountdown = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new TimeSet(), 0, 20 * 1);
        }

        //Startet TabListSync
        if(Bukkit.getOnlinePlayers().size() == 1) {
            plugin.tablistsync = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new TabListSync(), 0, 20*2);
        }

        if(!MySQL.isInDataBase(p)) {
            NewbieLoc.teleportnewbie(p);
        } else {
            SpawnLoc.teleportspawn(p);
        }

        Bukkit.getOnlinePlayers().forEach(player -> {
            player.hidePlayer(p);
            player.setPlayerListName(null);
            p.hidePlayer(player);
            p.setPlayerListName(null);
        });
    }

    @EventHandler
    public void cancelChat(PlayerChatEvent chat) {
        chat.setCancelled(true);
    }
}
