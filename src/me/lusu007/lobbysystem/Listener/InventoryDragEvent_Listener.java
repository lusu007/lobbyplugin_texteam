package me.lusu007.lobbysystem.Listener;

import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryDragEvent;

/**
 * Created by Lukas on 05.01.2015.
 */
public class InventoryDragEvent_Listener implements Listener {

    private final Main plugin;

    public InventoryDragEvent_Listener(Main main) {
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }


    @EventHandler
    public void onInventoryDrag(InventoryDragEvent e) {
        Player p = (Player) e.getWhoClicked();

        if(p.hasPermission("server.inventory.drag")) {
            e.setCancelled(false);
        } else {
            e.setCancelled(true);
        }
    }
}
