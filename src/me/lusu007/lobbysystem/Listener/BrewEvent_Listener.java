package me.lusu007.lobbysystem.Listener;

import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.BrewEvent;

/**
 * Created by Lukas on 05.01.2015.
 */
public class BrewEvent_Listener implements Listener {

    private final Main plugin;

    public BrewEvent_Listener(Main main) {
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }


    @EventHandler
    public void onBrew(BrewEvent e) {
        e.setCancelled(true);
    }
}
