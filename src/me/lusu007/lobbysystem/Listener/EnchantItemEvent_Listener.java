package me.lusu007.lobbysystem.Listener;

import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.enchantment.EnchantItemEvent;

/**
 * Created by Lukas on 04.01.2015.
 */
public class EnchantItemEvent_Listener implements Listener {

    private final Main plugin;

    public EnchantItemEvent_Listener(Main main) {
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }


    @EventHandler
    public void onEnchantItem(EnchantItemEvent e) {
        e.setCancelled(true);
    }
}
