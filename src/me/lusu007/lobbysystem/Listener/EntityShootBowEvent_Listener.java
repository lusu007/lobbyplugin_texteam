package me.lusu007.lobbysystem.Listener;

import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;

/**
 * Created by Lukas on 05.01.2015.
 */
public class EntityShootBowEvent_Listener implements Listener {

    private final Main plugin;

    public EntityShootBowEvent_Listener(Main main) {
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }


    @EventHandler
    public void onEntityShootBow(EntityShootBowEvent e) {
        e.setCancelled(true);
    }
}
