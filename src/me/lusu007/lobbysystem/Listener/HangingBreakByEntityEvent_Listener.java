package me.lusu007.lobbysystem.Listener;

import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;

/**
 * Created by Lukas on 05.01.2015.
 */
public class HangingBreakByEntityEvent_Listener implements Listener {

    private final Main plugin;

    public HangingBreakByEntityEvent_Listener(Main main) {
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }


    @EventHandler
    public void onHangingBreakByEntity(HangingBreakByEntityEvent e) {
        e.setCancelled(true);
    }
}
