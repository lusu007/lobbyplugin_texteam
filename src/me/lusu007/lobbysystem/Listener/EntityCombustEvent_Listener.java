package me.lusu007.lobbysystem.Listener;

import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityCombustEvent;

/**
 * Created by Lukas on 04.01.2015.
 */
public class EntityCombustEvent_Listener implements Listener {

    private final Main plugin;

    public EntityCombustEvent_Listener(Main main) {
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }


    @EventHandler
    public void onEntityCombust(EntityCombustEvent e) {
        e.setCancelled(true);
    }
}
