package me.lusu007.lobbysystem.Listener;

import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;

/**
 * Created by Lukas on 16.02.2015.
 */
public class CreatureSpawnEvent_Listener implements Listener {

    private final Main plugin;

    public CreatureSpawnEvent_Listener(Main main) {
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }

    @EventHandler
    public void onSpawn (CreatureSpawnEvent e) {
        e.setCancelled(true);
    }
}
