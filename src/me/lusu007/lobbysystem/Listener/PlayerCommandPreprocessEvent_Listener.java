package me.lusu007.lobbysystem.Listener;

import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

/**
 * Created by Lukas on 05.01.2015.
 */
public class PlayerCommandPreprocessEvent_Listener implements Listener {

    private final Main plugin;

    public PlayerCommandPreprocessEvent_Listener(Main main) {
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }


    @EventHandler
    public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent e) {
        Player p = e.getPlayer();

        if(e.getMessage().equalsIgnoreCase("/?") | e.getMessage().equalsIgnoreCase("help")) {
            p.sendMessage("§3===========[§6Hilfe§3]===========");
            p.sendMessage("§3/spawn - §6Teleportiert dich zum Spawn");
            p.sendMessage("§3==============================");
        }

        if(e.getMessage().equalsIgnoreCase("/pl")) {
            if(p.hasPermission("server.admin.command")) {
                e.setCancelled(false);
            } else {
                e.setCancelled(true);
                p.sendMessage(plugin.noperm);
            }
        }

        if(e.getMessage().equalsIgnoreCase("/plugin")) {
            if(p.hasPermission("server.admin.command")) {
                e.setCancelled(false);
            } else {
                e.setCancelled(true);
                p.sendMessage(plugin.noperm);
            }
        }

        if(e.getMessage().equalsIgnoreCase("/nuke")) {
            e.setCancelled(true);
        }
    }
}
