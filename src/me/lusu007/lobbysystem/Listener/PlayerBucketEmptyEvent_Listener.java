package me.lusu007.lobbysystem.Listener;

import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerBucketEmptyEvent;

/**
 * Created by Lukas on 05.01.2015.
 */
public class PlayerBucketEmptyEvent_Listener implements Listener {

    private final Main plugin;

    public PlayerBucketEmptyEvent_Listener(Main main) {
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }


    @EventHandler
    public void onPlayerBucketEmpty(PlayerBucketEmptyEvent e) {
        Player p = e.getPlayer();

        if(p.hasPermission("server.build.place")) {
            e.setCancelled(false);
        } else {
            e.setCancelled(true);
        }
    }
}
