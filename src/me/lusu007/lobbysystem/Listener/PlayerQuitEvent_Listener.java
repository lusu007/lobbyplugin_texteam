package me.lusu007.lobbysystem.Listener;

import me.lusu007.lobbysystem.Main.Main;
import me.lusu007.lobbysystem.Methoden.InventoryClear;
import me.lusu007.lobbysystem.Methoden.TabList;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * Created by Lukas on 04.01.2015.
 */
public class PlayerQuitEvent_Listener implements Listener {

    private final Main plugin;

    public PlayerQuitEvent_Listener(Main main) {
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }


    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        Player p = e.getPlayer();

        InventoryClear.clearEffects(p);
        InventoryClear.removeBoots(p);

        TabList.loadLobbyTablist(p);

        plugin.onlineplayer.remove(p.getName());

        e.setQuitMessage(null);

        //Beendet den Scheduler für die Zeitgleichsetzung von der Lobby und der Realität
        if(Bukkit.getOnlinePlayers().size() == 0) {
            Bukkit.getScheduler().cancelTask(plugin.timecountdown);
        }

        //Beendet TabListSync
        if(Bukkit.getOnlinePlayers().size() == 0) {
            Bukkit.getScheduler().cancelTask(plugin.tablistsync);
        }
    }
}
