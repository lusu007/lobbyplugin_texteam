package me.lusu007.lobbysystem.Listener;

import me.lusu007.lobbysystem.Main.Main;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerLeashEntityEvent;

/**
 * Created by Lukas on 05.01.2015.
 */
public class PlayerLeashEntityEvent_Listener implements Listener {

    private final Main plugin;

    public PlayerLeashEntityEvent_Listener(Main main) {
        this.plugin = main;
        plugin.getServer().getPluginManager().registerEvents(this, main);
    }


    @EventHandler
    public void onPlayerLeashEntity(PlayerLeashEntityEvent e) {
        e.setCancelled(true);
    }
}
